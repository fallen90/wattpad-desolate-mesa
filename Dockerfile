FROM mhart/alpine-node:8.9.0

WORKDIR /app
COPY . .

RUN apk --no-cache --virtual add python build-base && \
	npm install --production --no-audit && \
	apk del build-base python 

EXPOSE 80

CMD ["npm", "start"]