# wattpad : desolate-mesa #

`desolate-mesa` are projects I've written when I was starting my path to being a programmer. Some were successful and some didnt have any traction at all, but the experience of learning new things was fun and exciting since you could create cool things and express what you imagine into reality.

### What is this repository for? ###

* Downloads wattpad books (individually for now)
* By chapter download in txt and html formats.

### How do I get set up? ###

* Clone and run `npm install` in the root directory
* `npm start` to start the app
* `npm run dev` to run for deployment
* `npm run deploy` to deploy to `now.sh`

### Contribution guidelines ###

* Writing tests
* Code review
* Issues

### Who do I talk to? ###

* Repo owner or admin