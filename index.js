let express = require('express');
let app = express();
let server = require('http').createServer(app);
let io = require('socket.io').listen(server);
let fs = require('fs');
let to_file = require('./lib/to_file');
let _ = require('underscore');
let path = require('path');

app.get('/', function(req, res) {
    return res.sendFile(__dirname + '/views/index.html');
});


app.get('/_health', function(req, res) {
    return res.status(200).send('OK');
});

app.get('/get/:part_id/:type/:title', function(req, res) {
    var filename = '/mnt/blockstorage/' + req.params.part_id + '.' + req.params.type;
    var ua = req.header('user-agent');
    // Check the user-agent string to identyfy the device. 
    if(/iphone|ipod|ipad/i.test(ua)) {
       if(req.params.type == 'txt'){
        res.setHeader('Content-type', "text/plain");
       } else if(req.params.type == 'html'){
        res.setHeader('Content-type', "text/html");
       }
    } else {
       res.setHeader('Content-type', "application/octet-stream");
    }
    
    res.setHeader('Content-disposition', 'attachment; filename=' + (req.params.title) + '.' + req.params.type);
    res.sendFile(filename);
});

io.sockets.on('connection', socket => {

    socket.emit('log', log('Connected to server!'));
    socket.emit('log', log('Server version ' + require('./package.json').version));
    socket.emit('debug', process.env);
    socket.emit('log', log('Waiting for user input...'));

    socket.on('user-input', data => {
        socket.emit('log', log('Received input ' + data + '...'));

        var part_id = extractPartId(data);

        if (part_id) {
            socket.emit('log', log('Processing input `' + part_id + '` ...'));

            var WattpadBook = require('./lib/wattpad_api.js');

            var book = new WattpadBook(part_id);

            book.get(msg => {
                socket.emit('log', {
                    msg: '',
                    book: book,
                    type: 'book_cover'
                });
            }, socket);
        } else {
            socket.emit('log', log('It should be in this format `https://www.wattpad.com/1234567-some-text-title`'));
        }
    });

    socket.on('download', data => {
        const sanitize = function(str) {
            return str.replace(/[^A-Za-z0-9]+/ig, '');
        }

        if (data && Array.isArray(data.parts) && data.parts.length > 0) {
            socket.emit('log', log('starting download ....'));
            const book_download_id = ((new Date().getTime()) * Math.random()).toFixed(0);
            const book_name = data.book;
            const book_filename = sanitize(book_name);
            _.each(['html', 'txt'], ext => {
                const files = data.parts;
                const book_location = path.join('/mnt/blockstorage/', `${book_download_id}.${ext}`);
                let book_title_header_txt = `[ ${book_name} ]\n-------------------------------\n\n`;
                let book_title_header_html = `<h1>[ ${book_name} ]</h1><br/><hr><br/><br/>`

                let contents = [((ext == 'txt') ? book_title_header_txt : book_title_header_html)];

                _.each(files, file => {
                    let par = extractParts(file);
                    let partid = par.partid;
                    let title = par.title;
                    let filename = path.join('/mnt/blockstorage/', partid + '.' + ext);
                    let filecontents = fs.readFileSync(filename).toString('utf8');
                    let chapter_header_txt = `[ ${title} ]\n-------------------------------\n\n`;
                    let chapter_header_html = `<h2>[ ${title} ]</h2><br><hr><br><br>`;
                    let chapter_header = (ext == 'html') ? chapter_header_html : chapter_header_txt;
                    let chapter_ending = (ext == 'html') ? '<br></br>' : '\n\n';

                    filecontents = `${chapter_header}${filecontents}${chapter_ending}`;

                    contents.push(filecontents);
                });

                to_file(book_location, contents.join((ext == 'html') ? '<br/>' : '\n'));
            });

            socket.emit('log', {
                msg: '[ >>> ] ' + 'Download full book.',
                links: {
                    txt: '/get/' + book_download_id + '/txt/' + encodeURIComponent(book_filename),
                    html: '/get/' + book_download_id + '/html/' + encodeURIComponent(book_filename)
                },
                type: 'link'
            });
        }
    })
});


function log(string) {
    return {
        msg: '[ >>> ] ' + string,
        type: 'string'
    };
}

function pre(obj) {
    return '<pre>' + JSON.stringify(obj, null, 4) + '</pre>';
}

function extractPartId(url) {
    let matches = url.match(/(?:http|https):\/\/(?:www|m).wattpad.com\/([0-9]+)\-.+/i);
    if (matches && matches.length > 1) {
        return matches[1];
    } else {
        return false;
    }
}

function extractParts(filepath) {
    const file_parts = filepath.split('/');
    return {
        partid: file_parts[2],
        title: file_parts[4].replace(/-/g, ' ').replace('chptr ', '')
    };
}
server.listen(process.env.PORT || 80, function() {
    console.log('>>>', 'Server is listening in', process.env.PORT || 80)
});