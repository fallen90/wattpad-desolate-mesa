require('./format');

const req = require('sync-request');
const request = require('request');
const cheerio = require('cheerio');
const to_file = require('./to_file');
const tmp_location = '/mnt/blockstorage/';
const _ = require('underscore');
const sanitize = function(str) {
    return str.replace(/[^A-Za-z0-9]+/ig, '');
}

global.count = 0;

class WattpadApi {
    constructor(fields, part_id, part_key, parts_length, socket) {
        this.socket = socket;
        this.base_url = "https://www.wattpad.com/v4/parts/{0}?fields=" + encodeURIComponent(fields);
        this.base_url = this.base_url.format(part_id);
        this.part_id = part_id;

        this.parts_length = parts_length;
        this.part_key = (part_key + 1);
    }
    getBook(done) {
        var self = this;

        try {
            let msg = 'Finished fetching book {0}';

            var doRequest = function() {
                request(self.base_url, function(err, req, body) {
                    if (body) {
                        var book = JSON.parse(body.toString('utf8')).group;

                        done(book, msg.format(book.id));
                    } else {
                        console.log('[XXX]', 'failed retrying...', err, body);
                        doRequest();
                    }
                });
            }
            doRequest();

        } catch (ex) {
            done(null, '[ xxx ] Failed to fetch book');
        }
    }

    getPart(done) {
        var self = this;
        try {
            let msg = '[{0}/{1}] Finished fetching part {2} - {3}';

            request(self.base_url, function(err, req, body) {
                var part = body.toString('utf8');

                global.count++;

                msg = msg.format(count, self.parts_length, self.part_id);
                msg = msg.replace('{3}', '(Chptr ' + self.part_key + ') ' + JSON.parse(part).title);
                done(part, { msg, progress : ( count / self.parts_length )});

                //check if parts_length == part_key
                if (count == self.parts_length) {
                    global.count = 0; //reset global counter;
                    done(1, 'Finished Download.');
                }

            });
        } catch (ex) {
            done(null, '[ xxx ] Failed to fetch part ' + self.part_id);
        }
    }
}

class WattpadBook {
    constructor(part_id) {
        var self = this;
        self.fields = "group(id,title,cover,user(username,name,avatar),parts(title,id))";
        self.part_id = part_id;
    }

    get(done, socket) {
        var self = this;
        var api = new WattpadApi(self.fields, self.part_id, 0, 0);
        api.getBook(handler);

        function handler(book, msg) {

            if (typeof book !== 'undefined') {
                Object.assign(self, book);
            }

            if (typeof self.parts != 'undefined' && Array.isArray(self.parts)) {

                let parts_length = self.parts.length;
                socket.emit('log', log('Starting to fetch book parts, this may not be in order...'));
                _.each(self.parts, (part, key) => {

                    var wPart = new WattpadPart(part.id, key, parts_length);

                    wPart.get(function(part, msg) {
                        if (part != null) {
                            self.parts[key] = part;
                            var txt = self.parts[key].getText();
                            var html = self.parts[key].getHTML();

                            to_file(tmp_location + part.id + '.txt', txt);
                            to_file(tmp_location + part.id + '.html', html);

                            socket.emit('log', {
                                msg: '[ >>> ] ' + msg.msg,
                                progress : msg.progress,
                                key : key,
                                links: {
                                    txt: '/get/' + part.id + '/txt/' + encodeURIComponent('chptr-' + (key + 1) + '-' + sanitize(part.title)),
                                    html: '/get/' + part.id + '/html/' + encodeURIComponent('chptr-' + (key + 1) + '-' + sanitize(part.title))
                                },
                                type: 'link'
                            });
                        } else if(msg == 'Finished Download.'){ 
                            socket.emit('log', {
                                msg : '[>>] Finished Download. Download the whole book or download it one by one',
                                type : 'finished'
                            });
                        } else {
                            socket.emit('log', log(msg));
                        }
                    });
                });

            }

            if (typeof self.user != 'undefined') {
                self.user = new WattpadAuthor(self.user);
            }


            done(msg);
        }
    }
}


class WattpadPart {
    constructor(part_id, key, parts_length) {
        var self = this;
        self.fields = "id,title,text_url";
        self.part_id = part_id;
        self.key = key;
        self.parts_length = parts_length;
    }

    get(done) {
        var self = this;
        var api = new WattpadApi(self.fields, self.part_id, self.key, self.parts_length);

        api.getPart(function(part, msg) {
            if (typeof part !== 'undefined' && part != 1) {
                Object.assign(self, JSON.parse(part));
                self.getContents();

                done(self, msg);
            } else {
                done(null, msg);
            }
        });
    }

    getContents() {
        this.contents = req('GET', this.text_url.text).getBody('utf8');
    }

    getHTML() {
        var $ = cheerio.load(this.contents);

        $('p').each(function() {
            $(this).removeAttr('data-p-id');
        });

        return $.html();
    }

    getText() {
        var $ = cheerio.load(this.contents);
        return $.text();
    }
}


class WattpadAuthor {
    constructor(user) {
        Object.assign(this, user);
    }
}


function log(string) {
    return {
        msg: '[ >>> ] ' + string,
        type: 'string'
    };
}

module.exports = WattpadBook;