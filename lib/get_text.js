var cheerio = require('cheerio');

module.exports = function(text_url, done) {
    request({ url: text_url })
        .pipe(require('zlib').createGunzip())
        .on('data', function(data) {
            var $ = cheerio.load(data.toString());

            //cleanup
            $('p').each(function() {
                $(this).removeAttr('data-p-id');
            });

           	done($.html());
        });
}
